import React from 'react'
import { Helmet } from "react-helmet"
import 'typeface-inconsolata'
import 'normalize.css'
import '../styles/index.css'
import 'swiper/css/swiper.css'
import data from '../data/data.js'

//* Containers
import MediaContentRow from '../containers/MediaContentRow/MediaContentRow'

//* Components
import Header from '../components/header/header'
import Footer from '../components/footer/footer'
import Media from '../components/media/media'
import { IntroContent, ProjectContent } from '../components/content/content'

class IndexPage extends React.Component {
  state = {
    ...data
  }

  render() {
    return (
      <div>
        <Helmet>
          <meta charSet="utf-8" />
          <meta http-equiv="x-ua-compatible" content="ie=edge" />
          <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
          <title>{this.state.page.title}</title>
          <link rel="canonical" href={this.state.page.canonical} />
          <meta name="author" content={this.state.page.author} />
          <meta name="theme-color" content={this.state.page.themeColor} />
          <meta name="Description" content={this.state.page.description} />
          <meta name="Keywords" content={this.state.page.keywords} />
        </Helmet>
        <Header {...this.state.header} />
        {
          this.state.contents.map((content, i) => {
            return (
              <MediaContentRow key={content.id} reverse={i % 2 !== 0 || false}>
                <Media {...content.data}/>
                {
                  content.type === 'intro' ? <IntroContent {...content.data}/> : <ProjectContent {...content.data} />
                }
                
              </MediaContentRow>
            )
          })
        }
        <Footer {...this.state.footer} />
      </div>
    )
  }
}

export default IndexPage
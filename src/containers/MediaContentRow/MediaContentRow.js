import React from 'react'

import classes from './MediaContentRow.module.scss'

class MediaContentRow extends React.Component {
  render() {
    return (
      <div className={[classes.el, this.props.reverse ? classes.reverse : null].join(' ')}>
        {this.props.children}
      </div>
    )
  }
}

export default MediaContentRow
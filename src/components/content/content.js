import React from 'react'

import classes from './content.module.scss'

const IntroContent = props => {
  return (
    <div className={[classes.el, classes.intro].join(' ')}>
      <div className={classes.container}>
        <div className={classes.title}>{props.title}</div>
        <h1 className={classes.name}>{props.name}</h1>
        <div className={classes.copy} dangerouslySetInnerHTML={{__html: props.copy}}></div>
        <div className={classes.button}>
          <a href={props.download_resume.href} target="_blank" rel="noopener noreferrer">
            {props.download_resume.text}
          </a>
        </div>
      </div>
    </div>
  )
}

const ProjectContent = props => {
  return (
    <div className={[classes.el, classes.project].join(' ')}>
      <div className={classes.container}>
        <h2 className={classes.title}>{props.title}</h2>
        <div className={classes.skills}>{props.skills}</div>
        <div className={classes.copy} dangerouslySetInnerHTML={{__html: props.copy}}></div>
        <div className={classes.link}>
          {
            props.links.map(link => {
              return (
                <a href={link.href} target="_blank" rel="noopener noreferrer" key={link.id}>
                  <span className="icon-external-link"></span>
                  {link.text}
                </a>
              )
            })
          }
        </div>
      </div>
    </div>
  )
}

export {
  IntroContent,
  ProjectContent
}